include .env
include .versions.env

VERSION = $@

VERSION_JAR = $($(VERSION)_JAR)
VERSION_JAVA = $($(VERSION)_JAVA)

JAVA_VERSION = $(if $(VERSION_JAVA),$(VERSION_JAVA),17-slim)

BUILD_ARGS = --build-arg SERVER_JAR=$(VERSION_JAR)
BUILD_ARGS += --build-arg JAVA_VERSION=$(JAVA_VERSION)

ENV_FILES = --env-file .env
COMPOSE_FILES = -f docker-compose.yml

DOCKER_COMPOSE = docker-compose $(ENV_FILES) $(COMPOSE_FILES)

all: $(VERSIONS)

$(VERSIONS):
	docker build $(BUILD_ARGS) -t $(IMAGE):$@ ./server

up:
	$(DOCKER_COMPOSE) up -d

kill:
	$(DOCKER_COMPOSE) kill

down:
	$(DOCKER_COMPOSE) down

log:
	$(DOCKER_COMPOSE) logs -f

shell:
	$(DOCKER_COMPOSE) run --rm --entrypoint sh server -c sh

permissions:
	sudo chown -R $(USER):$(USER) .

clean:
	sudo rm -rf ./data/server/world ./data/server/logs

redo:
	make kill down clean up permissions
