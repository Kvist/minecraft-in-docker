# Minecraft Server in Docker

## Supported versions

Technically, all server versions back to 1.2 can be built, but all of them run on Java 11, which might cause problems, so if you should run into something, feel free to open an issue and we will look into that particlar server version.

- `1.17.1`
- `1.17`
- `1.16.5`
- `1.16.4`
- `1.16.3`
- `1.16.2`
- `1.16.1`
- `1.16`
- `1.15.2`
- `1.15.1`
- `1.15`
- `1.14.4`
- `1.14.3`
- `1.14.2`
- `1.14.1`
- `1.14`
- `1.13.2`
- `1.13.1`
- `1.13`
- `1.12.2`
- `1.12.1`
- `1.12`
- `1.11.2`
- `1.11.1`
- `1.11`
- `1.10.2`
- `1.10.1`
- `1.10`
- `1.9.4`
- `1.9.3`
- `1.9.2`
- `1.9.1`
- `1.9`
- `1.8.9`
- `1.8.8`
- `1.8.7`
- `1.8.6`
- `1.8.5`
- `1.8.4`
- `1.8.3`
- `1.8.2`
- `1.8.1`
- `1.8`
- `1.7.10`
- `1.7.9`
- `1.7.8`
- `1.7.7`
- `1.7.6`
- `1.7.5`
- `1.7.4`
- `1.7.3`
- `1.7.2`
- `1.6.4`
- `1.6.2`
- `1.6.1`
- `1.5.2`
- `1.5.1`
- `1.4.7`
- `1.4.6`
- `1.4.5`
- `1.4.4`
- `1.4.2`
- `1.3.2`
- `1.3.1`
- `1.2.5`
- `1.2.4`
- `1.2.3`
- `1.2.2`
- `1.2.1`

## Contributing

If you want to contribute, there are several things you could do:

### Add a new version

Adding a new version is incredibly easy. Simply add the version's name to where it belongs into the VERSIONS variable in the `.versions.env` file. Vanilla versions should always come last, with the latest vanilla version being the last one in the list.

After you've added the new version, you need to add a corresponding `${VERSION}_JAR` variable to the same file. The value of this variable needs to be a URL where the JAR can be downloaded.

You're basically done now, time to test your setup. Build and run your new version by running

```sh
make <version>
```

and run it with 

```sh
RUN=<version> make up log
```


### Provide another Java version for a specific server version

By default, all server versions are built on the `16-alpine` version of `openjdk`. If you encounter compatibility issues with this Java version and a server version, simply add a `${VERSION}_JAVA` variable to the `.versions.env` specifying an image tag of an `openjdk` version that works. Run and build the image.

